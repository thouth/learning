﻿using System;
using System.Linq;
using OnlineStore.Database.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace OnlineStore.Database
{
    public class DatabaseContext : IdentityDbContext<User>
    {
        public DatabaseContext()
        {

        }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=onlinestore;Username=postgres;Password=1");
        }

        public DbSet<Product> Products { get; set; }
    }
}
