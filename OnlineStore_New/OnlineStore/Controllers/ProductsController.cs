﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OnlineStore.Database.Models;
using OnlineStore.Database;

namespace OnlineStore.Controllers
{
    public class ProductsController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public string Add(string title, decimal price)
        {
            title = title.ToLower();
            title = title.Trim();

            // Цена не должна быть больше 0.
            if (price <= 0)
            {
                return "error";
            }

            var context = new DatabaseContext();


            var sameProduct = context.Products.FirstOrDefault(prod => prod.Title.ToLower() == title);
            var productAlreadyExists = sameProduct != null;

            if (productAlreadyExists)
                return "уже есть";


            var newProduct = new Product();
            newProduct.Title = title;
            newProduct.Price = price;


            context.Products.Add(newProduct);
            context.SaveChanges();


            return "success";
        }
    }
}