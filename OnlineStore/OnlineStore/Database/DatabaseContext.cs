﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OnlineStore.Controllers;

namespace OnlineStore.Database
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Product> Products { get; set; }


        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
        }


        //public DatabaseContext()
        //{
        //    Database.EnsureCreated();
        //}
    }
}
