﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace OnlineStore.Controllers
{
    public class ProductController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public string GetString()
        {
            return "строка";
        }

        public List<Product> GetList()
        {
            //var array = new Product[10];



            var list = new List<Product>();


            //var product1 = new Product(1, "Майка");

            var product1 = new Product
            {
                Id = 1,
                Name = "Майка",
                Price = 100
            };


            var product2 = new Product
            {
                Id = 2,
                Name = "Футболка",
                Price = 200
            };


            list.Add(product1);
            list.Add(product2);


            var nameProduct1 = product1.Name;


            return list;
        }


        public string Add(int id, string name, decimal price)
        {
            // Создаем на основе переданных параметров из браузера.
            var product = new Product
            {
                Id = id,
                Name = name,
                Price = price
            };


            // Добавляем в базу.



            return "Success";
        }
    }


    public class Product
    {
        //public Product(int id, string name)
        //{
        //    Id = id;
        //    Name = name;
        //}

        public int Id { get; set; }

        public string Name { get; set; }

        public decimal Price { get; set; }
    }
}